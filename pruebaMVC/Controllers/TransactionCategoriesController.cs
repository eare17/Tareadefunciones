﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pruebaMVC.Models;

namespace pruebaMVC.Controllers
{
    public class TransactionCategoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TransactionCategories
        public async Task<ActionResult> Index()
        {
            return View(await db.TransactionCategories.ToListAsync());
        }

        // GET: TransactionCategories/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransactionCategory transactionCategory = await db.TransactionCategories.FindAsync(id);
            if (transactionCategory == null)
            {
                return HttpNotFound();
            }
            return View(transactionCategory);
        }

        // GET: TransactionCategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TransactionCategories/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description")] TransactionCategory transactionCategory)
        {
            if (ModelState.IsValid)
            {
                db.TransactionCategories.Add(transactionCategory);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(transactionCategory);
        }

        // GET: TransactionCategories/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransactionCategory transactionCategory = await db.TransactionCategories.FindAsync(id);
            if (transactionCategory == null)
            {
                return HttpNotFound();
            }
            return View(transactionCategory);
        }

        // POST: TransactionCategories/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description")] TransactionCategory transactionCategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transactionCategory).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(transactionCategory);
        }

        // GET: TransactionCategories/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransactionCategory transactionCategory = await db.TransactionCategories.FindAsync(id);
            if (transactionCategory == null)
            {
                return HttpNotFound();
            }
            return View(transactionCategory);
        }

        // POST: TransactionCategories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TransactionCategory transactionCategory = await db.TransactionCategories.FindAsync(id);
            db.TransactionCategories.Remove(transactionCategory);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
