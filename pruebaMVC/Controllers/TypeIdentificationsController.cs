﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pruebaMVC.Models;

namespace pruebaMVC.Controllers
{
    public class TypeIdentificationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TypeIdentificationIds
        public async Task<ActionResult> Index()
        {
            return View(await db.TypeIdentifications.ToListAsync());
        }

        // GET: TypeIdentificationIds/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeIdentification typeIdentificationId = await db.TypeIdentifications.FindAsync(id);
            if (typeIdentificationId == null)
            {
                return HttpNotFound();
            }
            return View(typeIdentificationId);
        }

        // GET: TypeIdentificationIds/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TypeIdentificationIds/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Description,Active")] TypeIdentification typeIdentificationId)
        {
            if (ModelState.IsValid)
            {
                db.TypeIdentifications.Add(typeIdentificationId);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(typeIdentificationId);
        }

        // GET: TypeIdentificationIds/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeIdentification typeIdentificationId = await db.TypeIdentifications.FindAsync(id);
            if (typeIdentificationId == null)
            {
                return HttpNotFound();
            }
            return View(typeIdentificationId);
        }

        // POST: TypeIdentificationIds/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Description,Active")] TypeIdentification typeIdentificationId)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeIdentificationId).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(typeIdentificationId);
        }

        // GET: TypeIdentificationIds/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeIdentification typeIdentificationId = await db.TypeIdentifications.FindAsync(id);
            if (typeIdentificationId == null)
            {
                return HttpNotFound();
            }
            return View(typeIdentificationId);
        }

        // POST: TypeIdentificationIds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TypeIdentification typeIdentificationId = await db.TypeIdentifications.FindAsync(id);
            db.TypeIdentifications.Remove(typeIdentificationId);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
