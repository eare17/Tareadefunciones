﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using pruebaMVC.Models;

namespace pruebaMVC.Controllers
{
    public class PersonsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index2()
        {

            ViewBag.TypeIdentificationId = new SelectList(db.TypeIdentifications.OrderBy(p => p.Name), "Id", "Name");
            return View();
        }

        public JsonResult GetPerson()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Person> output = db.Persons.ToList();
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        // GET: Personas
        public ActionResult Index(string sort, string search, int? page)
        {
            ViewBag.NameSort = String.IsNullOrEmpty(sort) ? "name_desc" : string.Empty;
            ViewBag.ApellidoSort = sort == "apellido" ? "apellido_desc" : "apellido";
            ViewBag.TipoIdentificacionSort = sort == "tipoidentificacion" ? "tipoidentificacion_desc" : "tipoidentificacion";
            ViewBag.IdentificacionSort = sort == "identificacion" ? "identificacion_desc" : "identificacion";

            ViewBag.CurrentSort = sort;
            ViewBag.CurrentSearch = search;

            IQueryable<Person> personas = db.Persons;

            if (!String.IsNullOrEmpty(search))
                personas = personas.Where(p => p.FirstName.Contains(search) || p.LastName.Contains(search) || p.Identification.Contains(search) 
                    || p.TypeIdentifications.Name.Contains(search));

            switch (sort)
            {
                case "name":
                    personas = personas.OrderBy(p => p.FirstName)
                                        .ThenBy(p => p.LastName);
                    break;

                case "name_desc":
                    personas = personas.OrderByDescending(p => p.FirstName)
                                        .ThenBy(p => p.LastName);
                    break;

                case "apellido":
                    personas = personas.OrderBy(p => p.LastName).ThenBy(p => p.FirstName);

                    break;
                case "apellido_desc":
                    personas = personas.OrderByDescending(p => p.LastName).ThenBy(p => p.FirstName);
                    break;

                case "tipoidentificacion":
                    personas = personas.OrderBy(p => p.TypeIdentifications.Name)
                                        .ThenBy(p => p.FirstName)
                                        .ThenBy(p => p.LastName);
                    break;

                case "tipoidentificacion_desc":
                    personas = personas.OrderByDescending(p => p.TypeIdentifications.Name)
                                        .ThenBy(p => p.FirstName)
                                        .ThenBy(p => p.LastName);;
                    break;

                case "identificacion":
                    personas = personas.OrderBy(p => p.Identification);

                    break;
                case "identificacion_desc":
                    personas = personas.OrderByDescending(p => p.Identification);
                    break;

                default:
                    personas = personas.OrderBy(p => p.FirstName)
                                        .ThenBy(p => p.LastName);
                    break;
            }

            int pageSize = 5;
            int pageNumber = page ?? 1;         //si viene nulo asignar pagina 1

            //return View(await personas.ToListAsync());
            return View(personas.ToPagedList(pageNumber, pageSize));
        }

        // GET: Personas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person persona = await db.Persons.FindAsync(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // GET: Personas/Create
        public ActionResult Create()
        {
            ViewBag.TypeIdentificationId = new SelectList(db.TypeIdentifications.OrderBy(p => p.Name), "Id", "Name");

            return View();
        }

        // POST: Personas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FirstName,LastName,TypeIdentificationId,Identification,Active")] Person persona)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Persons.Add(persona);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    var e = ex.GetBaseException() as SqlException;
                    if (e != null)
                        switch (e.Number)
                        {
                            case 2601:
                                TempData["MessageToClient"] = String.Format("DATOS DUPLICADOS DEL REGISTRO A GUARDAR.");
                                break;
                            default:
                                throw;
                        }
                }
            }

            return View(persona);
        }

        // GET: Personas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person persona = await db.Persons.FindAsync(id);

            ViewBag.TypeIdentificationId = new SelectList(db.TypeIdentifications.OrderBy(p => p.Name), "Id", "Name", persona.TypeIdentificationId);

            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Personas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,FirstName,LastName,TypeIdentificationId,Identification,Active")] Person persona)
        {
            if (ModelState.IsValid)
            {
                db.Entry(persona).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.TypeIdentificationId = new SelectList(db.TypeIdentifications.OrderBy(p => p.Name), "Id", "Name", persona.TypeIdentificationId);
            return View(persona);
        }

        // GET: Personas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person persona = await db.Persons.FindAsync(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Personas/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Person persona = await db.Persons.FindAsync(id);
            db.Persons.Remove(persona);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
