﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pruebaMVC.Models
{
    [Table("TransactionCategory", Schema = "adm")]  //nombre de la tabla en la bd 
    public class TransactionCategory
    {
        [Key]   //definir que es una llave
        public int Id { get; set; }

        [Index("INDEX_ADM_TransactionCategory", IsUnique = true)]	//hacer campo unico, no se puede repetir
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre de la transacción")]
        [StringLength(30, ErrorMessage = "El nombre de la categoria no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Categoria")]
        public string Name { get; set; }

        [StringLength(250, ErrorMessage = "La descripcion de la Categoria debe ser 250 caracteres o menos.")]
        [Display(Name = "Descripcion de la Categoria")]
        public string Description { get; set; }

        //public virtual ICollection<TransactionType> TransactionTypes { get; set; }
    }
}