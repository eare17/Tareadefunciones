﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace pruebaMVC.Models
{
    [Table("person", Schema = "adm")] 
    public class Person
    {
        [Key]
        public int Id { get; set; }

        [Index("INDEX_ADM_PERSONA", 1, IsUnique = true)]	
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre de la persona.")]
        [StringLength(30, ErrorMessage = "El nombre de la persona no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Nombre")]
        public string FirstName { get; set; }

        [Index("INDEX_ADM_PERSONA", 2, IsUnique = true)]

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el apellido de la persona.")]
        [StringLength(30, ErrorMessage = "El apellido de la persona no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        public int TypeIdentificationId { get; set; }

        [Index("INDEX_ADM_PERSONA_IDENTIFICACION", IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar la identificacion de la personae.")]
        [StringLength(20, ErrorMessage = "La identificacion de la persona no debe ser mayor de 20 caracteres.")]
        [Display(Name = "Identificacion")]
        public string Identification { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        public virtual TypeIdentification TypeIdentifications { get; set; }
        //public virtual ICollection<TypeIdentificationId> TypeIdentificationIds { get; set; }

    }
}