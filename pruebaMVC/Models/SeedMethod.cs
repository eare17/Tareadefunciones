﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace pruebaMVC.Models
{
    public class SeedMethod
    {
        private ApplicationDbContext _ctx;

        public SeedMethod(ApplicationDbContext ctx)
        {
            _ctx = ctx;
        }

        public void Seed(ApplicationDbContext context)
        {
            context.TypeIdentifications.AddOrUpdate(ti => ti.Name,
                new TypeIdentification {Name = "Cedula",Description = "Cedula",Active = true},
                new TypeIdentification{Name = "Pasaporte",Description = "Pasaporte",Active = true},
                new TypeIdentification{Name = "Licencia de conducir",Description = "Licencia de conducir",Active = true}
            );

            context.SaveChanges();

            TypeIdentification typeidentificationcedula = context.TypeIdentifications.First(ti => ti.Name == "Cedula");
            TypeIdentification typeidentificationpassport = context.TypeIdentifications.First(ti => ti.Name == "Pasaporte");
            TypeIdentification typeidentificationlicence = context.TypeIdentifications.First(ti => ti.Name == "Licencia de conducir");

            context.Persons.AddOrUpdate(p => new { p.FirstName, p.LastName},
                new Person{FirstName = "Enmanuel",LastName = "Rodriguez",TypeIdentificationId = typeidentificationcedula.Id,Identification = "001-181294-0006F",Active = true},
                new Person{FirstName = "Maria de los Angeles",LastName = "Ruiz",TypeIdentificationId = typeidentificationpassport.Id,Identification = "001-200596-0025A",Active = true},
                new Person{FirstName = "Roberto",LastName = "Mendoza",TypeIdentificationId = typeidentificationlicence.Id,Identification = "001-050689-00036F",Active = true}
            );

            context.SaveChanges();
        }
    }
}