﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pruebaMVC.Models
{
    [Table("estudiante", Schema = "adm")] 
    public class Estudiante
    {
        [Key]  
        public int Id { get; set; }

        [Index("INDEX_ADM_ESTUDIANTE",1,IsUnique = true)]	
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre del estudiante.")]
        [StringLength(30, ErrorMessage = "El nombre del estudiante no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Index("INDEX_ADM_ESTUDIANTE",2,IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el apellido del estudiante.")]
        [StringLength(30, ErrorMessage = "El apellido del estudiante no debe ser mayor de 30 caracteres.")]
        [Display(Name = "Apellido")]
        public string Apellido { get; set; }

        [Index("INDEX_ADM_ESTUDIANTE",3, IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el carnet del estudiante.")]
        [StringLength(20, ErrorMessage = "El carnet del estudiante no debe ser mayor de 20 caracteres.")]
        [Display(Name = "Carnet")]
        public string Carnet { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar la carrera del estudiante.")]
        [Display(Name = "Carrera")]
        public string Carrera { get; set; }

        [Index("INDEX_ADM_ESTUDIANTE_IDENTIFICACION", IsUnique = true)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar la identificacion del estudiante.")]
        [StringLength(20, ErrorMessage = "La identificacion del estudiante no debe ser mayor de 20 caracteres.")]
        [Display(Name = "Identificacion")]
        public string Identificacion { get; set; }

    }
}